const { response , request} = require('express')
const DataPersona = require('../data/datos-persona')
const Persona = require('../models/Persona')

/*
const lista = [
  {
    id: 1,
    nombre: "charly",
    edad: 23,
    domicilio: "cbba"
  },
  {
    id: 2,
    nombre: "ever",
    edad: 23,
    domicilio: "cbba"
  }
]
*/

const dataPerson = new DataPersona()

const personaGet = (req, res = response) => {

  let datos = dataPerson.getPersonas()

  res.json({
    msg: 'datos :',
    datos
  })
}

const personaPost = (req = request, res = response) => {
  const { nombre, apellidos, ci, direccion, sexo } = req.body;

  let personaNueva = new Persona(nombre, apellidos, ci, direccion, sexo)

  dataPerson.addPersona(personaNueva)

  res.json({
    msg: 'Persona agregada',
  })
}

const personaPut = (req = request, res = response) => {
  const {id} = req.params
  const { nombre, apellidos, ci, direccion, sexo } = req.body;

  const index = dataPerson.data.findIndex(object => object.id === id)

  if (index !== -1) {
    dataPerson.editarPersona({ nombre, apellidos, ci, direccion, sexo }, index)
    res.json({
      msg: 'persona editada con exito'
    })
  } else {
    res.json({
      msg: 'persona no encontrada'
    })
  }
}

const personaDelete = (req = request, res = response) => {
  const {id} = req.params

  const index = dataPerson.data.findIndex(object => object.id === id)

  if (index !== -1) {
    dataPerson.eliminarPersona(index)
    res.json({
      msg: 'persona eliminada con exito'
    })
  } else {
    res.json({
      msg: 'persona no encontrada'
    })
  }
}

module.exports = {
  personaGet,
  personaPost,
  personaPut,
  personaDelete
}