const express = require('express');

class Server {

  constructor() {
    this.app = express();
    this.port = process.env.PORT || 8080;
    this.personPath = "/api/persona"

    this.middlewares()
    this.routes();
  }

  middlewares() {
    //Se publica la carpeta public
    this.app.use(express.json())
    //this.app.use(express.static('public'));
  }

  routes() {
    this.app.use(this.personPath, require('../routes/personas'))
    /*
    this.app.get('/', (req, res) => {
      res.send('Hello ')
    });
    
    this.app.post('/', (req, res) => {
      res.send('Post ')
    });
    
    this.app.put('/', (req, res) => {
      res.send('Put')
    });
    */
  }

  listen() {
    //Con process.env.PORT obtenemos el valor de la variable PORT que esta en el archivo .env
    this.app.listen(this.port, () => {
      console.log('Servidor corriendo en puerto ', this.port);
    });
  }
}

module.exports = Server